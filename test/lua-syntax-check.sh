#!/usr/bin/env sh

LUA_FILES="plugins/busshark.dissectors"
# Luacheck options, see: https://luacheck.readthedocs.io/en/stable/config.html
LUA_CHECK_CONFIG="test/luacheckrc"
# luacheck is required
if ! [ -x "$(command -v luacheck)" ]; then
    echo "luacheck not found" >&2
    exit 1
fi

# CLI: https://luacheck.readthedocs.io/en/stable/cli.html
luacheck --config $LUA_CHECK_CONFIG $LUA_FILES

# Return code = 0 -> No warnings, no errors
# Return code = 1 -> Some warnings, no errors
# Return code > 1 -> Some warnings and errors
if [ $? -eq 0 -o $? -eq 1 ]; then
    echo "Luacheck passed!"
    exit 0
else
    echo "Luacheck found some errors: " >&2
    exit 1
fi
