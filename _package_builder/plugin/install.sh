#!/usr/bin/env sh

TARGET_BUSSHARK_FILES_MOD=755
SCRIPT_FOLDER=$(dirname "$0")
SOURCE=${SOURCE:-"${SCRIPT_FOLDER}"}
TARGET_PLUGINS_FOLDER=${1:-"${HOME}/.local/lib/wireshark/plugins"}
TARGET_PROFILES_FOLDER=${2:-"${HOME}/.config/wireshark/profiles"}

if [ "$1" = "--global" ]; then 

    TARGET_PLUGINS_FOLDER="/usr/lib/wireshark/plugins"
    TARGET_PROFILES_FOLDER="/usr/share/wireshark/profiles"

fi

# enforce intstall target folder
mkdir -p "${TARGET_PLUGINS_FOLDER}"
mkdir -p "${TARGET_PROFILES_FOLDER}"

# remove old version
rm -rf "${TARGET_PLUGINS_FOLDER}/busshark."*
rm -rf "${TARGET_PROFILES_FOLDER}/busshark."*

# set file mod
chmod -f -R ${TARGET_BUSSHARK_FILES_MOD} "${SOURCE}/plugins" "${SOURCE}/profiles"

# copy files to target folders
cp -r "${SOURCE}/plugins/busshark."* "${TARGET_PLUGINS_FOLDER}"
cp -r "${SOURCE}/profiles/busshark."* "${TARGET_PROFILES_FOLDER}"
