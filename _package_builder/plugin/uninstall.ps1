﻿param(
  [string] $Destination = "$env:appdata\Wireshark",
  [bool] $Global = $false
)

switch ( $Global ) {
  $false { $Destination = $Destination }
  $true { $Destination = "$env:programfiles\Wireshark" }
  default { $Destination = $Destination }
}

Remove-Item "$Destination\plugins\busshark.dissectors.mvb" -Recurse -Force
Remove-Item "$Destination\plugins\busshark.dissectors" -Recurse -Force
Remove-Item "$Destination\profiles\busshark.mvb" -Recurse -Force
Remove-Item "$Destination\profiles\busshark.wtb" -Recurse -Force