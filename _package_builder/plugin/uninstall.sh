#!/usr/bin/env sh

TARGET_PLUGINS_FOLDER=${1:-"${HOME}/.local/lib/wireshark/plugins"}
TARGET_PROFILES_FOLDER=${2:-"${HOME}/.config/wireshark/profiles"}

if [ "$1" = "--global" ]; then 

    TARGET_PLUGINS_FOLDER="/usr/lib/wireshark/plugins"
    TARGET_PROFILES_FOLDER="/usr/share/wireshark/profiles"

fi

rm -rf "${TARGET_PLUGINS_FOLDER}/busshark."*
rm -rf "${TARGET_PROFILES_FOLDER}/busshark."*
