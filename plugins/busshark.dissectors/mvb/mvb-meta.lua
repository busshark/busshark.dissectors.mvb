local meta = {}

-- Extract meta information from frame
--
function meta.extract(frame, fill_header)
  meta.size = (frame:len() / 2) + 8
  meta.code_size = meta.size - 16
  meta.status_codes = frame(16, meta.code_size)

  if fill_header.payload_version > 2 then
    meta.size = meta.size + 1;
    meta.code_size = meta.size - 18
    meta.status_codes = frame(18, meta.code_size)
  end

  meta.firstCode = meta.status_codes(0, 1)
  meta.first_code_uint = meta.firstCode:uint()
  meta.frame_type = bit.band(meta.first_code_uint, 0x03)
end

return meta
