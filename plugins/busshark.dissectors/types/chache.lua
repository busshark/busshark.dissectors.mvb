local api = {}


function api.new()
    return {}
end

function api.get(
    source, -- [[cache table]]
    ...     -- [[ cahce keys]]
)
    local args = { ... }

    local item = source

    for _, key in ipairs(args) do
        item = item[key]
        if item == nil then
            return nil
        end
    end

    return item
end

function api.update(
    source, -- [[cache table]]
    value,  -- [[ cache entry]]
    ...     -- [[ cachce keys]]
)
    local args = { ... }
    local arg_size = #args
    local item = source

    for i, key in ipairs(args) do
        if i == arg_size then
            item[key] = value
            break
        end

        local new_item  = item[key]
        if new_item == nil then
            new_item = {}
            item[key] = new_item
        end

        item = new_item
    end

    -- luacheck: push ignore source
    source = item;
    -- luacheck: pop
end

return api
