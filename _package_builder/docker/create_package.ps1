param(
  [string] $Source = ".",
  [string] $Destination = ".",
  [string] $PkgsName = "busshark.dissectors",
  [string] $PkgsVersion = "1.0.0"
)

New-Item -Path "$Destination\example-pcap-files" -Type Directory -Force
Copy-Item -Path "$Source\example-pcap-files/*" -Destination "$Destination\example-pcap-files"

docker build -f "${PSScriptRoot}/Dockerfile" --tag "linuxserver.wireshark.${PkgsName}:${PkgsVersion}" --no-cache --build-arg PACKAGE_FILE="${PkgsName}-${PkgsVersion}.zip" --build-arg EXAMPLE_FOLDER="example-pcap-files" "${Destination}"